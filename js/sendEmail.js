  var CLIENT_ID = '430399266184-jimdjr71fcmunjde1lhldgdv11b5dvmj.apps.googleusercontent.com';

      // Array of API discovery doc URLs for APIs used by the quickstart
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

      // Authorization scopes required by the API; multiple scopes can be
      // included, separated by spaces.
      var SCOPES =
        'https://www.googleapis.com/auth/gmail.readonly '+
        'https://www.googleapis.com/auth/gmail.send';


      /**
       *  On load, called to load the auth2 library and API client library.
       */
      function handleClientLoad() {
        gapi.load('client:auth2', initClient);
      
      }

      /**
       *  Initializes the API client library and sets up sign-in state
       *  listeners.
       */
      function initClient() {
        gapi.client.init({
          discoveryDocs: DISCOVERY_DOCS,
          clientId: CLIENT_ID,
          scope: SCOPES
        }).then(function () {
          // Listen for sign-in state changes.
          gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

          // Handle the initial sign-in state.
          updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        });
      }

      /**
       *  Called when the signed in status changes, to update the UI
       *  appropriately. After a sign-in, the API is called.
       */
      function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
         
          listLabels();
        } else {
          gapi.auth2.getAuthInstance().signIn();
        }
      }

      /**
       *  Sign in the user upon button click.
       */

      /**
       * Append a pre element to the body containing the given message
       * as its text node. Used to display the results of the API call.
       *
       * @param {string} message Text to be placed in pre element.
       */
      function appendPre(message) {
          console.log(message)
      }

      /**
       * Print all Labels in the authorized user's inbox. If no labels
       * are found an appropriate message is printed.
       */
      function listLabels() {
        gapi.client.gmail.users.labels.list({
          'userId': 'me'
        }).then(function(response) {
          var labels = response.result.labels;
          appendPre('Labels:');

          if (labels && labels.length > 0) {
            for (i = 0; i < labels.length; i++) {
              var label = labels[i];
              appendPre(label.name)
            }
          } else {
            appendPre('No Labels found.');
          }
        });
      }
	  function sendMessage(headers_obj, message, callback)
	  {
	    var email = '';
	    for(var header in headers_obj)
	      email += header += ": "+headers_obj[header]+"\r\n";
	    email += "\r\n" + message;
	    var sendRequest = gapi.client.gmail.users.messages.send({
	      'userId': "me",
	      'resource': {
	        'raw': window.btoa(email).replace(/\+/g, '-').replace(/\//g, '_')
	      }
	    });
	    return sendRequest.execute(callback);
	  }
	function sendEmailtoUs()
      {
        $('#sendEmail').addClass('disabled');
        sendMessage(
          {
            'To': "rojasv907@gmail.com",
            'Subject': "propuesta"
          },
          $('#name').val()+" "+$('#email').val() +" "+$('#subject').val()+" "+$('#message').val(),
          composeTidy
        );
        return false;
      }
      function composeTidy()
      {
        $('#name').val('');
        $('#email').val('');
        $('#subject').val('');
        $('#message').val('');
        $('#sendEmail').removeClass('disabled');
      }